<?php /** This PHP File is auto-generated from theme option. Please do not edit this file directly**/ ?>
<?php
##Start your code below this line

add_action('genesis_before', function() { ?>
<iframe height="0" style="display:none;height:0px" src="//community.denturewearers.com/token"></iframe>
<style>.menu-primary1 {display: none}</style>
<script>
( function($) {
    window.addEventListener("message", function(t) {
    if ( t.data.logged == 1 || t.data.logged == 0 ) {
        console.log( t.data.logged, );
        if( t.data.logged == 1 ) {
            $('#menu-header-menu').find('.unlog').css({'display': 'none'});
            $('#menu-header-menu').append('<li class="menu-item user-info"><a href="https://community.denturewearers.com/settings/profile" class="user-icon"><img alt="My Account" src="'+t.data.img +'"></a></li>');
        }
    }
    //console.log('test');
});
})(jQuery)
</script>
<?php

//$request = dw_curl('https://community.dw.six-20.com/api/me');
//echo '<pre>', print_r( $request, 1), '</pre>';

});

add_action('wp_enqueue_scripts', function () {
    $mode = CMK::v('cmk-site-build-mode');
    wp_enqueue_script( 'slick', CMK::$cmk_url. '/js/round.js' );
    if ( $mode === '1' ) return;
    wp_enqueue_style('bulma-css', Dynatheme::$cmk_url. '/css/bulma-col.min.css');
}, 20);

add_action( 'wp_print_styles', function() {
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'dynatheme-2' );
}, 100 );

add_filter('cmk/register/dynathemejs', function( $defs ) {
    $defs['deps'] = ['jquery'];
    return $defs;
});

add_action('genesis_entry_content', function() {
    if ( !is_single() || is_post_type('dw-stories') ) return;
    echo '<div class="feat-img"><a href="'.get_permalink().'">';
    echo get_the_post_thumbnail( get_the_ID(), 'medium_large', array( 'class' => 'aligncenter' ) );
    echo '</a></div>';
}, 1);

add_action('genesis_before_header', function() {
    echo '<div class="menu-icon mob-only"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 17h-12v-2h12v2zm0-4h-12v-2h12v2zm0-4h-12v-2h12v2z"/></svg></div>';
});

add_action('genesis_header', function() {
    echo '<span class="close-menu mob-only"><svg id="close" width="100%" height="100%"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg></svg></span>';
});

add_shortcode('find-doctor', function() {
    return '<div class="sec to-5 ftd" style="background:#1b1a20;color: #fff;padding:3em 0 0"><div class="wrap">
        <div class="columns is-vcentered">
        <div class="column is-6">
        <h3 class="topl" style="color: #fff; font-size: 48px; font-weight: 400;">Find Top Dental Professionals</h3>
        <p>Search for dental professionals near you.</p>
        <form action="https://community.denturewearers.com/search-dental-professionals" method="GET">
        <div class="columns is-1 is-variable">
        <div class="column is-9"><input type="text" name="zip" placeholder="Enter zip code" /></div>
        <div class="column is-3"><button type="submit" class="btn btn-wbg" style="width: 100%; text-align: center;">Find</button></div>
        </div>
        </form>
        </div>
        <div class="column is-6">
        <div class="doc">
        <div class="doc-info">
        <h6 class="name"></h6>
        <p></p>
        </div>
        <div><img src="https://denturewearers.com/wp-content/uploads/2020/02/girl-doc.jpg" alt="Doctors Name" /></div>
        </div>
        </div>
        </div>
        </div></div>';
});

add_action('wp_head', function() {
    if ( is_post_type('lifestyle') || is_tax('topic') ) {
        add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
        
        if ( is_archive() ) {
            
            add_filter( 'genesis_post_info', function( $pi ) {
                $pi = '[post_date format="j F Y"] [post_edit]';
                return $pi;
            });
            
            add_action('genesis_after_header', function() {
                _lifestyle_tips_banner();
            }, 0);
            
            add_filter( 'genesis_attr_content', function( $atts ) {
                 $atts['class'] .= ' columns is-multiline is-variable is-6 topics';
                 return $atts;
            });
            
            add_action( 'genesis_before_entry', function() {
                 echo '<div class="column is-4 single-topic">';
            });
            
            add_action( 'genesis_after_entry', function() {
                 echo '</div>';
            });
            
            add_action( 'genesis_before_entry', function() {
                echo '<a href="'.get_permalink().'" class="topic-img">';
                echo get_the_post_thumbnail( get_the_ID(), 'medium_large', array( 'class' => 'aligncenter' ) );
                echo '</a>';
            });
            
            
            add_action( 'pre_get_posts',  function( $q) {
                $q->set( 'posts_per_page', '9' );
            });
            
            add_action( 'genesis_entry_header', function() {
                echo '<p class="topic-cat">'. _get_tax(get_the_ID(), 'topics' ).'</p>';
            },1);
            remove_action('genesis_entry_content', 'genesis_do_post_content');
            remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
        } else {
            remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
            remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
            remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
            add_filter( 'genesis_post_info', function ($post_info) {
                return '';
            });
            
            add_action('genesis_after_header', function() {
                _lifestyle_tips_single_banner();
            }, 0);
            
            /*add_action( 'genesis_before_entry', function() {
                echo '<a href="'.get_permalink().'" class="topic-img">';
                echo get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'aligncenter' ) );
                echo '</a>';
            });*/
            
            add_action('genesis_after_content_sidebar_wrap', function() {
                echo '<div class="related-topics post-type-archive-lifestyle sec">';
                echo '<h3>Related <strong>Tips:</strong></h3>';
                echo _cmk_related_post( get_the_ID() );
                echo '</div>';
            });
        }
    }
    
    if ( is_post_type('dw-stories') ) {
        add_action('genesis_after_header', function() {
            if ( is_single() ) {
                $img = get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'alignnone' ) );
                echo do_shortcode('[cmwrap cl="sec to-1" is="background:#1A1920; color: #fff;padding: 7em 0"]
                <div class="columns">
                <div class="column"><div class="post-img">'.$img.'</div></div>
                <div class="column is-narrow" style="width: 45px;"></div>
                <div class="column">
                    <h1 class="topl" style="color: #fff;font-size: 60px;font-weight: 400;">Featured <br>Testimonials</h1>
                    <a class="btn btn-wbg" href="/submit-personal-story/">SUBMIT YOUR STORY</a>
                </div>
                </div>
                [/cmwrap]');
            } else {
                echo do_shortcode('[cmwrap cl="sec to-1" is="background: url(/wp-content/uploads/2020/07/personal-stories.jpg) no-repeat 0 0 #1A1920; color: #fff;padding: 7em 0"]
                <div class="columns">
                <div class="column">
                    <h1 class="topl" style="color: #fff;font-size: 60px;font-weight: 400;">Featured <br>Testimonials</h1>
                    <a class="btn btn-wbg" href="/submit-personal-story/">SUBMIT YOUR STORY</a>
                </div>
                <div class="column bgl"></div>
                </div>
                [/cmwrap]'); 
            }
        });
        
        if ( is_archive() ) {
            remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
            remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
            remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
            add_action('genesis_entry_content', function() {
                $id = get_the_ID();
                echo '<div class="story-single">
                    <div class="columns is-vcentered is-variable is-8">
                        <div class="column is-4">
                            '.get_the_post_thumbnail( $id, 'medium_large', array( 'class' => 'alignnone' ) ).'
                        </div>
                        <div class="column">
                            <h2><a href="'.get_permalink($id).'">'.get_the_title( $id ).'</a></h2>
                            <time class="entry-time">'.get_post_time('j F Y').'</time>
                            <div class="summary">'.substr(wp_strip_all_tags(get_the_content()), 0, 300).'</div>
                            <div class="more bgl sss">
                                <a class="btn btn-wbg" href="'.get_permalink($id).'">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>';
            });
        }
        add_filter( 'genesis_post_info', function( $pi ) {
            return;
        });
        add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
    }

    if ( is_post_type('faqs') ) {
       add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_sidebar_content' );
        
        if ( is_archive() ) {
            remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
            remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
            remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
            add_action('genesis_entry_content', function() {
                $id = get_the_ID();
                echo '<div class="story-single aaaaa">
                    <div class="columns is-vcentered is-variable is-8">
                        <div class="column">
                            <h2 class=""><a href="'.get_permalink($id).'">'.get_the_title( $id ).'</a></h2>
                            <time class="entry-time">'.get_post_time('j F Y').'</time>
                            <div class="summary">'.substr(wp_strip_all_tags(get_the_content()), 0, 300).'</div>
                            <div class="more bgl sss">
                                <a class="btn btn-wbg" href="'.get_permalink($id).'">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>';
            });
            add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
        }
        add_filter( 'genesis_post_info', function( $pi ) {
            return;
        });
        // add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_sidebar_content' ); //this one
    }


});

add_action( 'pre_get_posts',  function( $q) {
    if ( !is_admin() && $q->is_main_query() && is_post_type_archive( 'lifestyle' ) ) {
        $q->set( 'posts_per_page', '9' );
    }
});

function _lifestyle_tips_banner() { ?>
<div class="sec sec-1" style="background:url(/wp-content/uploads/2020/03/tips-banner.jpg) no-repeat center center; color: #fff;padding: 12em 0">
    <div class="wrap">
        <div class="columns is-variable is-8 is-vcentered">
            <div class="column is-6" style="margin-top: 7em;">
                <h1 class="topl" style="color: #fff; font-size: 60px; font-weight: 500;">Lifestyle Tips</h1>
                <p style="font-size: 24px;">Everything About Living with Dentures</p>
            </div>
            <div class="column"></div>
            <div class="column is-narrow" style="width: 445px; margin: 0 auto;">
                <div class="cwtd bgl">
                    <h2 style="font-size: 32px;">Connect with Top Dental Professionals</h2>
                    <p>Dental professionals reviewed by real patients.</p>
                    <a class="btn btn-wbg" href="https://community.denturewearers.com/?view=2">Connect Today</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sec sec-t2" style="padding: 4em 0 1em;">
    <div class="wrap">
        <h2 class="topl" style="margin-bottom: 30px; font-size: 60px; font-weight: 500;">Topics</h2>
        <p style="font-size: 20px;font-weight: 700">Filter by Topics</p>
        <ul class="filters">
            <li><a href="/lifestyle-tips/" class="btn <?php echo is_tax() ? '' : 'active' ;?>">All</a></li>
            <?php echo _pull_lifestyle_cat(); ?>
        </ul>
    </div>
</div>
<?php
}

function _pull_lifestyle_cat() {
    $cats = get_terms( 'topics', ['hide_empty' => false] );
    if ( $cats && is_array($cats ) ) {
        $li = '';
        foreach ($cats as $cat => $val) {
            $cl = get_queried_object()->term_id == $val->term_id ? ' active' : '' ;
            $li .= '<li><a href="'.get_term_link( $val).'" class="btn'.$cl.'">'. $val->name.'</a></li>'."\n";
        }
        return $li;
    }
}

function _lifestyle_tips_single_banner() {?>
<div class="sec sec-t1" style="padding: 3em 0">
    <div class="wrap">
        <p class="breadcrumb"><a href="/lifestyle-tips">Lifestyle tips</a> > <?php echo _get_tax(get_the_ID(), 'topics', 1, 1) ?></p>
        <h1 style="margin-bottom: 20px; font-size: 36px; font-weight: 500;"><?php the_title() ?></h1>
    </div>
</div>
<?php
}

function _get_tax($id, $name, $one = true, $link = 0) {
    $term_list = get_the_terms( $id, $name );
    if ( $term_list ) {
        if ( $one ) {
             if ( $link ) return '<a href="'.get_term_link($term_list[0]).'">'.$term_list[0]->name.'</a>';
             else return $term_list[0]->name;
        }
        return join(', ', wp_list_pluck($term_list, 'name'));
    }
}

function _cmk_related_post( $id ) {
    $args = [
        'post_type' => 'lifestyle',
        'posts_per_page' => 3,
        'post__not_in' => [$id],
        'orderby'   => 'rand'
    ];
    
    $query = new WP_QUery( $args );
    $posts = $query->get_posts();
    
    if ( $posts ) { //If no post on First query Run Second Query
        $div = '<div class="columns is-multiline is-variable is-6 topics">';
        foreach( $posts as $post => $p ) {
            $div .= _post_archive_template( $p->ID );
        }
        return $div. '</div>';
    }
}

function _post_archive_template( $id ) {
    if ( 'publish' != get_post_status ( $id ) ) return;
    ob_start(); ?>
<div class="column is-4 single-topic">
    <a href="<?php the_permalink($id) ?>" class="topic-img"><?php echo get_the_post_thumbnail( $id, 'medium_large', array( 'class' => 'aligncenter' ) );?></a>
    <article class="post-292 lentry" itemscope="" itemtype="https://schema.org/CreativeWork">
        <p class="topic-cat"><?php echo _get_tax( $id, 'topics') ?></p>
        <header class="entry-header">
            <h2 class="entry-title" itemprop="headline"><a class="entry-title-link" rel="bookmark" href="<?php the_permalink($id) ?>"><?php echo get_the_title( $id )?></a></h2>
            <p class="entry-meta"><time class="entry-time" itemprop="datePublished"><?php echo get_post_time('j F Y')?></time> </p>
        </header>
    </article>
</div>
    <?php
    return ob_get_clean();
}

add_shortcode('dw-reviews', function() {
    $args = [
        'post_type' => 'dw-reviews',
        'posts_per_page' => 10,
    ];
    
    $terms = get_terms( [
        'taxonomy' => 'treatments',
        'hide_empty' => false
    ]);
    
    $out = '<div class="columns jvallno is-vcentered">';
    if ( $terms ) {
        $out .= '<div class="column is-4"><div class="rev-nav"><ul>';
        foreach( $terms as $index => $term ) {
            $active = $term->term_id == 17 ? ' active': '';
            $out .= '<li><a class="nav-item'.$active .'" href="#rev-'.$term->term_id.'">'.$term->name.'</a></li>';
            $query = new WP_QUery( $args + ['taxonomy' => 'treatments','term' => $term->slug]);
            $terms[$index]->posts = $query->get_posts();
        }
        $out .= '</ul></div></div>';
    }

    //echo '<pre>', print_r($terms, 1), '</pre>';
    $out .= '<div class="column">';
    foreach ($terms as $key => $term) {
        if ( $term->posts ) {
            $active = $term->term_id == 17 ? ' active': '';
            $out .= '<div id="rev-'.$term->term_id.'" class="rev-cont rc-'.$term->term_id.$active.'">';
            foreach ($term->posts as $key => $post) {
                $out .= '<div class="dwimg"><div class="columns jvallnos is-vcentered is-gapless is-mobile test">';
                $out .= '<div class="column is-narrow" style="width: 162px;"><img src="'.get_the_post_thumbnail_url($post->ID,'full').'" alt="DW"></div>';
                $out .= '<div class="column">';
                $out .= '<p class="nm">'.$post->post_excerpt.'</p>';
                $out .= '</div>';
                $out .= '</div></div>';
            }
            $out .= '</div>';
        }
    }
    return $out. '</div></div>';
});

add_shortcode('home-lt', function() {
    $lt = [
        1 => [
            'name' => 'Eating with Dentures',
            'link' => '/topics/eating-with-dentures/'
        ],
        2 => [
            'name' => 'Cleaning Dentures',
            'link' => '/topics/cleaning-methods/'
        ],
        3 => [
            'name' => 'Essential Oral Hygiene Info',
            'link' => '/topics/essential-oral-hygiene/'
        ],
        4 => [
            'name' => 'Oral Infections',
            'link' => '/topics/oral-infections/'
        ],
        5 => [
            'name' => 'Dry Mouth',
            'link' => '/topics/dry-mouth/'
        ],
        6 => [
            'name' => 'Soreness',
            'link' => '/topics/soreness/'
        ]
    ];
    
    $out = '<div class="columns is-multiline is-gapless">';
    foreach ( $lt as $k => $t) {
        $out .= '<div class="column is-6">';
            $out .= '<a href="'.$t['link'].'"><div class="lt-cont">';
                $out .= '<div class="lt-icon i-'.$k.'"></div>';
                $out .= '<h5>'.$t['name'].'</h5>';
            $out .= '</div></a>';
        $out .= '</div>';
    }
    return $out.'</div>';
});

/** FAQ **/ 


function _cmk_related_topics( $id = 0, $tax_id = false, $tax_id_topic = 0 ) {
    
    $args = [
        'post_type' => 'faqs',
        'posts_per_page' => -1,
        'orderby'   => 'menu_order',
        'order'   => 'ASC'
    ];
    
    if ( $tax_id ) {
        $args['tax_query'] = [
            'relation' => 'AND',
            [
                'taxonomy' => 'dwr-userguide',
                'field'    => 'term_taxonomy_id',
                'terms'    => $tax_id
            ],
            [
                'taxonomy' => 'dwr-usertopics',
                'field'    => 'term_id',
                'terms'    =>  $tax_id_topic
            ]
        ];
    } 
    
    $query = new WP_QUery( $args );
    $posts = $query->get_posts();
    //echo '<pre>', print_r($posts, 1), '</pre>';
    if ( $posts ) {
        $div = '<h3>Articles in this section</h3><ul class="related-topic">';
        foreach( $posts as $post => $p ) {
            $active = $id == $p->ID ? ' active': '';
            $div .= '<li class="item'.$active.'"><a href="'.get_permalink( $p->ID ).'">'.get_the_title( $p->ID ).'</a></li>';
        }
        return $div. '</ul>';
    }
}

add_action('wp_head', function() {
    if ( is_post_type('faqs') && is_single() ) {
        add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
        add_action( 'genesis_before_entry', function() {
            $topics = get_the_terms( get_the_ID(), 'dwr-usertopics' );
            $guide = get_the_terms( get_the_ID(), 'dwr-userguide' );
            $topicsId = $topics ? $topics[0]->term_id : 0;
            $guideId = $guide ? $guide[0]->term_id: 0;
            echo '<div class="columns" style="background: #fff"><div class="column is-3 related-topics">';
            echo _cmk_related_topics( get_the_ID(), $guideId, $topicsId );
            echo '</div><div class="column">';
        });
        add_action( 'genesis_after_entry', function() {
            echo '</div></div>';
        });
    }  
});