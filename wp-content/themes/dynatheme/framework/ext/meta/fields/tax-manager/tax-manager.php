<?php 

// Render boxes
function cmb_tags_render( $field, $value, $object_id, $object_type, $field_type ) { 
	cmb_tags_enqueue(); ?>
	<?php echo $field_type->textarea( array(
		'desc' => '',
		'class' => 'show-if-no-js',
	) ); ?>
	<div class="ajaxtag hide-if-no-js">
		<input type="text" name="" class="new form-input-tip" size="16" autocomplete="off" value="" />
		<input type="button" class="button" value="<?php esc_attr_e('Add'); ?>" tabindex="3" />
	</div>
	<?php echo $field_type->_desc( true ); ?>
	<div class="tagchecklist hide-if-no-js"></div>
	<?php
}
add_filter( 'cmb2_render_tags', 'cmb_tags_render', 10, 5 );
add_filter( 'cmb2_render_tags_sortable', 'cmb_tags_render', 10, 5 );

// enque scripts
function cmb_tags_enqueue() {
	$base = DynaTheme::$cmk_url . '/framework/ext/meta/fields/tax-manager/';
	wp_enqueue_script( 'cmb_tags_script', $base. 'js/tags.js', array( 'jquery', 'jquery-ui-sortable' ), '' );
	wp_enqueue_style( 'cmb_tags_stype', $base  . 'css/tags.css', array(), '' );
}