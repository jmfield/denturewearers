<?php

require_once 'init.php';
require_once 'fields/select2/cmb-field-select2.php';
require_once 'fields/tax-manager/tax-manager.php';
require_once 'fields/search/search.php';
require_once 'fields/grid1/grid.php';


add_action( 'cmb2_render_tax_select', '_cmk_taxonomy_multiselector', 10, 5 );
function _cmk_taxonomy_multiselector( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {

	$current_value = get_post_meta($object_id, $field->args['id'], true); 
	if ( !isset($field->args['taxonomy']) ) {
		echo 'Please define "taxonomy" parameter';
		return;
	}

	$terms = get_terms([
		'taxonomy' => $field->args['taxonomy'],
		'hide_empty' => false,
	]);
	if ( is_wp_error( $terms ) ) {
		echo 'No terms found from '.$field->args['taxonomy'].' taxonomy';
		return;
	}
	//echo '<pre>', print_r( $current_value, 1), '</pre>';
	//echo '<pre>', print_r( $field, 1), '</pre>';
	$html = '<div class="_cmk_add_feature" style=" margin-bottom: 14px;text-align: center;"><input type="text" placeholder="Add new Feature" style="width: 400px;line-height: 1.6;vertical-align: top;margin: 0;"><button class="button button-primary button-large">Add Feature</button></div>';
	$html .= '<div class="row cmb2GridRow">';
	foreach ($terms as $key => $term) {
		$id = $term->term_id;
		$active = in_array($id, $current_value) ? ' checked' : '';
		$html .= '<div class="col-md-6" style="margin-bottom: 10px;">
		<input type="checkbox" name="'.$field->args['id'].'[]" value="'.$id.'" id="_cmktax_'.$id.'-'.$term->slug.'"'.$active.'/>
		<label for="_cmktax_'.$id.'-'.$term->slug.'">'. $term->name.'</label>
		</div>';
	}
	echo $html.'</div>';
}

add_action( 'cmb2_admin_init', 'cmk_script_css_metaboxes' );
function cmk_script_css_metaboxes() {
	$types = ['page', 'post'];
	$prefix = '_cmk_box_';
	
	$cmk_boxes = new_cmb2_box( array(
		'id'			=> '_cmk_metas',
		'title'         => __( 'Dynatheme Option', 'cmk' ),
		'object_types'  => apply_filters('cmk/register/metabox', $types), // Post type
		#'closed'		=> true,
		#'show_names'    => false, 
		'context'      => 'normal',
		'priority'     => 'core',
		'cmb_styles' => true,
	) );

	$cmk_boxes->add_field( array(
		'name' => __( 'Hide Title', 'cmk' ),
		'desc' => __( 'Do not show post title on the front-end', 'cmk' ),
		'id'   => $prefix . 'hide_title',
		'type' => 'checkbox',
		'class' => 'fullbox'
	));

	$cmk_boxes->add_field( array(
		'name'    => 'Load Script',
		'id'      => $prefix . 'enque_scripts',
		'desc'    => 'Load Additional Javascript. Drag to reorder',
		'type'    => 'pw_multiselect',
		'options' => CMK::get_cmk_unenqueue_lists(),
	));
	$f1 = $cmk_boxes->add_field( array(
		'name' => __( 'Header Script', 'cmk' ),
		'desc' => __( 'Added on wp_head() action hook. You can add any HTML tags in here like &lt;script&gt;&lt;/script&gt;, &lt;style&gt;&lt;/style&gt; etc.', 'cmk' ),
		'id'   => $prefix . 'wp_head',
		'default' => get_post_meta(get_the_ID(), 'CmkBoxes_headscript', false),
		'type' => 'textarea_code',
		'attributes' => array(
			'readonly' => false,
			'data-codeeditor' => json_encode( array(
				'htmlhint' => array(
					'mode' => 'html',
				),
			) ),
		),
		'class' => 'fullbox'
	));
	$f2 = $cmk_boxes->add_field( array(
		'name' => __( 'Footer Script', 'cmk' ),
		'desc' => __( 'Added on wp_footer() action hook. You can add any HTML tags in here like &lt;script&gt;&lt;/script&gt;, &lt;style&gt;&lt;/style&gt; etc.', 'cmk' ),
		'id'   => $prefix . 'wp_footer',
		'type' => 'textarea_code',
		'attributes' => array(
			'readonly' => false,
			'data-codeeditor' => json_encode( array(
				'htmlhint' => array(
					'mode' => 'html',
				),
			) ),
		),		
		'class' => 'fullbox'
	));
	$cmk_boxes->add_field( array(
		'name' => __( 'Display Scripts', 'cmk' ),
		'desc' => __( 'Display List of enqueue scripts in the front end separated by "|"', 'cmk' ),
		'id'   => $prefix . 'showenqueued_script',
		'type' => 'checkbox',
	));
	$cmk_boxes->add_field( array(
		'name' => __( 'Display Styles', 'cmk' ),
		'desc' => __( 'Display List of enqueue styles in the front end separated by "|"', 'cmk' ),
		'id'   => $prefix . 'showenqueued_styles',
		'type' => 'checkbox',
	));
	$cmk_boxes->add_field( array(
		'name' => __( 'Remove Scripts', 'cmk' ),
		'after' => __( 'Add the script handle to remove scripts on this page (separate by pipe "|")', 'cmk' ),
		'id'   => $prefix . 'remove_scripts',
		'type' => 'text',
	));
	$cmk_boxes->add_field( array(
		'name' => __( 'Remove Styles', 'cmk' ),
		'after' => __( 'Add the style handle to remove styles on this page (separate by pipe "|")', 'cmk' ),
		'id'   => $prefix . 'remove_styles',
		'type' => 'text',
	));

	$cmb2Grid = new \Cmb2Grid\Grid\Cmb2Grid($cmk_boxes);
	$row = $cmb2Grid->addRow();
	$row->addColumns([
		[$f1, 'class' => 'col-md-12'], [$f2, 'class' => 'col-md-12']
	]);

	do_action('cmk/metas', $cmk_boxes, $prefix);
	//custom_metas();
}
