<?php

/**
 *
 *Do Not add anything in here, put you customization under "Appearance > Dyantheme Options > Custom PHP"
 *
**/

if( file_exists( dirname( __FILE__ ) .'/framework/config/global-config.php')) {
  require_once( dirname( __FILE__ ) .'/framework/config/global-config.php');
}

add_filter('use_block_editor_for_post', '__return_false');